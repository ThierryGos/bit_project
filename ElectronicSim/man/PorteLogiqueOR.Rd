% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/PorteLogiqueOR.R
\name{PorteLogiqueOR}
\alias{PorteLogiqueOR}
\title{La Porte | (OR)
Simule le comportement d'une porte logique OR
Constituée de deux transistors NPN, cette porte logique ne laisse passer du courant
qu'à la condition qu'il n'y ai du courant sur au moins l'une des deux entrées.
On utilisera deux transistorNPNs montés en série La Base de chacun sera branch?s sur les entr?es E1 et E2
la tension du collecteur du premier sera branch? sur la valeur de l'emetteur, la tension de collecteur du second
sera branché sur la tension de l'emetteur du premier et la sortie sera branch? sur la tension emetteur du second
# Si E1 =  E2 =   Alors S =
         0      1          1
         1      0          1
         1      1          1
         0      0          0}
\usage{
PorteLogiqueOR(Entree1, Entree2, Emetteur = 5)
}
\arguments{
\item{Entree1}{A numeric containing the value at the door A}

\item{Entree2}{A numeric containing the value at the door B}

\item{Emetteur}{A numeric containing the value of the current that can be emitted}
}
\value{
The Emitting current
}
\description{
La Porte | (OR)
Simule le comportement d'une porte logique OR
Constituée de deux transistors NPN, cette porte logique ne laisse passer du courant
qu'à la condition qu'il n'y ai du courant sur au moins l'une des deux entrées.
On utilisera deux transistorNPNs montés en série La Base de chacun sera branch?s sur les entr?es E1 et E2
la tension du collecteur du premier sera branch? sur la valeur de l'emetteur, la tension de collecteur du second
sera branché sur la tension de l'emetteur du premier et la sortie sera branch? sur la tension emetteur du second
# Si E1 =  E2 =   Alors S =
         0      1          1
         1      0          1
         1      1          1
         0      0          0
}
\examples{
PorteLogiqueOR(Entree1=0.1,Entree2=0.1,Emetteur=5)
PorteLogiqueOR(Entree1=0,Entree2=0.1, Emetteur=5)
PorteLogiqueOR(Entree1=0,Entree2=0)
PorteLogiqueOR(Entree1=2,Entree2=0.1)
}
