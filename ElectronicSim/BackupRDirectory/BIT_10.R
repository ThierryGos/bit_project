# L'additionneur N Bits (n bit)
# L'additionneur N Bits est une s?rie de N additionneur complet
# Chaque chiffre A et B constitu?s de N bits (ex 3 bits) vont s'additionner 2 a 2
# EX : on veut additionner 010 et 110. Il faut additionner le 0 du premier bit de A avec le premier bit de B 0
# Ensuite la somme de cet additionneur sera la valeur du bit de la somme de A et B et la retenue sera
# propag?e dans le Cin de l'additionneur suivant qui sommera le deuxi?me bit de A (1) et celui de B (1),
# la retenue sera propag?e dans le 3eme additionneur complet qui sommera le troisi?me bit de A (0) et celui de B(1)

# IN : A et B les nombre a additionner en vecteur de boolean
# OUT : S la valeur de la somme sur n bits et C la retenue sur 1 bit
# EX : AdditionneurNbit(A=010,B=110) =>  c(5,0,0,0,0) avec un vecteur de format c(Retenue,S3,S2,S1)
# METHODE :
# On regarde A et B comme des vecteurs de booleans. La taille du plus grand des deux vecteurs
# d?termine le nombre N d'additionneur complet a utiliser.
# La premiere retenue est initialis?e ? 0 et on boucle l'additionneur N fois.
#
# FONCTION ----------------------------------------------------------------
AdditionneurNBits <- function(VectBoolA, VectBoolB){
  Result <- NULL
  if(!is.logical(VectBoolA) | !is.logical(VectBoolB)){
    Result <- 'ONE PARAMETER IS NOT A VECTOR OR IS NA'
  } else {
    CinRetenue <- 0
    max(c(length(VectBoolA),length(VectBoolB))) -> Nmax
    if(length(VectBoolA)<Nmax){
      append(VectBoolA,rep(F,Nmax-length(VectBoolA)),after=0) -> VectBoolA
    }else if(length(VectBoolB)<Nmax){
      append(VectBoolB,rep(F,Nmax-length(VectBoolB)),after=0) -> VectBoolB
    }
    VectNumA <- rep(0,Nmax)
    VectNumB <- rep(0,Nmax)
    VectNumA[VectBoolA] <- 1
    VectNumB[VectBoolB] <- 1
    N=1
    Result <- NULL
    for (i in seq(Nmax,1,-1)){
      AdditionneurComplet(VectNumA[i],VectNumB[i],Cin=CinRetenue) -> ResAddCompl
      CinRetenue <- ResAddCompl[2]
      c(ResAddCompl[1],Result) -> Result
      N+1 -> N
    }
    c(CinRetenue,Result) -> Result
  }
  return(Result)
}

# TESTS -------------------------------------------------------------------


# test la r?action attendue si la  fonction a un des param?tres d'entr?e qui n'est pas un numeric
AdditionneurNBits.test.Num <- function(){
  test_result = F
  if(identical(AdditionneurNBits(VectBoolA = as.character("Toto"),VectBoolB = c(F,T,F)),'ONE PARAMETER IS NOT A VECTOR OR IS NA') &
     identical(AdditionneurNBits(VectBoolA = c(F,T,F),VectBoolB = as.character("Toto")),'ONE PARAMETER IS NOT A VECTOR OR IS NA')){
    test_result = T
  }
  if(test_result){
    print("AdditionneurNBits.test.Num PASSED")
  }else {
    print("AdditionneurNBits.test.Num IS NOT PASSED")
  }
  return(test_result)
}

# test le comporement normal de la porte XOR
AdditionneurNBits.test.Value <- function(){
  test_result = F
  if(identical(AdditionneurNBits(VectBoolA =  c(F,T,T,F,T),VectBoolB = c(F,T,F,F,F,F,T)),c(Retenue=0,Somme=0,Somme=5,Somme=0,Somme=5,Somme=5,Somme=5,Somme=0)) &
     identical(AdditionneurNBits(VectBoolA =  c(F,F,T),VectBoolB = c(F,F,T)),c(Retenue=0,Somme=0,Somme=5,Somme=0)) &
     identical(AdditionneurNBits(VectBoolA =  c(F,T,T,T),VectBoolB = c(F,F,T)),c(Retenue=0,Somme=5,Somme=0,Somme=0,Somme=0))){
    test_result = T
  }
  if(test_result){
    print("AdditionneurNBits.test.Value PASSED")
  }else {
    print("AdditionneurNBits.test.Value IS NOT PASSED")
  }
  return(test_result)
}

#Enchaine tous les tests
AdditionneurNBits.test.AllTests <- function(){
  if(AdditionneurNBits.test.Num() &
     AdditionneurNBits.test.Value()){
    print(x = "ALL TESTS PASSED")
  } else print(x = "SOMETHING WENT WRONG !")
}

# Launch all tests
AdditionneurNBits.test.AllTests()
