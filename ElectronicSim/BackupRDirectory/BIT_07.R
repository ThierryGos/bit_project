# La Porte | (XOR)
# La porte logique XOR (-) marche selon le sch?ma logique suivant :
# Si E1 = ; E2 = ; Alors S =
#         0      0      0
#         1      0      1
#         0      1      1
#         1      1      0
# Pour que la tension passe ? la sortie il faut une tension minimal sur strictement une des deux entr?es
# IN : Les tensions des Entr?es E1 et E2 ainsi que de l'Emetteur
# OUT : une valeur de tension sur la sortie
# EX : PorteXOR(E1=0.2,E2=0,Emetteur=5) = 5
# METHODE : deux portes NOT, deux portes AND, et une porte OR. Les entr?es sont connect?es chacune a une porte NOT
# des deux entr?es d'une des porte AND, la Sortie de la porte NOT ?tant connect?e a l'autre entr?e de la porte AND.
# Enfin, les deux sorties des portes AND sont connect?e aux entr?es de la porte OR.

# Remarque : XOR le sh?rif, sh?rif de l'espae, XOR sur la terre c'est lui qui fait la loi.
# https://www.youtube.com/watch?v=dDb1cwai2Qk

# FONCTION ----------------------------------------------------------------
PorteLogiqueXOR <- function(Entree1, Entree2, Emetteur){
  result <- NULL
  if(!is.numeric(Entree1) | !is.numeric(Entree2) | !is.numeric(Emetteur)){
    result <- 'ONE PARAMETER IS NOT AN NUMERIC OR IS NA'
  } else {
    S_NOT1 <- PorteLogiqueNOT(Entree1 = Entree1)
    S_NOT2 <- PorteLogiqueNOT(Entree1 = Entree2)
    S_AND1 <- PorteLogiqueAND(Entree1 = S_NOT1,Entree2 = Entree2,Emetteur=5)
    S_AND2 <- PorteLogiqueAND(Entree1 = S_NOT2,Entree2 = Entree1,Emetteur=5)
    S_PorteOR <- PorteLogiqueOR(Entree1 = S_AND1,Entree2=S_AND2,Emetteur=5)
    result <- S_PorteOR
  }
  return(result)
}

# TESTS -------------------------------------------------------------------


# test la r?action attendue si la  fonction a un des param?tres d'entr?e qui n'est pas un numeric
PorteLogiqueXOR.test.Num <- function(){
  test_result = F
  if(identical(PorteLogiqueXOR(Entree1 = as.character("Toto"),Entree2 = 0.2,Emetteur = 5),'ONE PARAMETER IS NOT AN NUMERIC OR IS NA') &
     identical(PorteLogiqueXOR(Entree1 = 0.2,Entree2 = as.character("Toto"),Emetteur = 5),'ONE PARAMETER IS NOT AN NUMERIC OR IS NA') &
     identical(PorteLogiqueXOR(Entree1 = 0.2,Entree2 = 0.2,Emetteur = as.character("Toto")),'ONE PARAMETER IS NOT AN NUMERIC OR IS NA')){
    test_result = T
  }
  if(test_result){
    print("PorteLogiqueXOR.test.Num PASSED")
  }else {
    print("PorteLogiqueXOR.test.Num IS NOT PASSED")
  }
  return(test_result)
}

# test le comporement normal de la porte XOR
PorteLogiqueXOR.test.Value <- function(){
  test_result = F
  if(identical(PorteLogiqueXOR(Entree1 = 0.2,Entree2 = 0.2,Emetteur = 5),0) &
     identical(PorteLogiqueXOR(Entree1 = 0,Entree2 = 0.2,Emetteur = 5),5) &
     identical(PorteLogiqueXOR(Entree1 = 0.2,Entree2 = 0,Emetteur = 5),5) &
     identical(PorteLogiqueXOR(Entree1 = 0,Entree2 = 0,Emetteur = 5),0)){
    test_result = T
  }
  if(test_result){
    print("PorteLogiqueXOR.test.Value PASSED")
  }else {
    print("PorteLogiqueXOR.test.Value IS NOT PASSED")
  }
  return(test_result)
}

#Enchaine tous les tests
PorteLogiqueXOR.test.AllTests <- function(){
  if(PorteLogiqueXOR.test.Num() &
     PorteLogiqueXOR.test.Value()){
    print(x = "ALL TESTS PASSED")
  } else print(x = "SOMETHING WENT WRONG !")
}

# Launch all tests
PorteLogiqueXOR.test.AllTests()
