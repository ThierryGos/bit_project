# L'additionneur complet (1 bit)
# L'additionneur complet marche selon le sch?ma suivant :
#     Si A =; B =; etCin;Alors S=; et C
#         0     	0   	0	    0	    0
#         0     	0	    1    	1	    0
#         0	      1	    0	    1	    0
#         0	      1	    1	    0	    1
#         1	      0	    0	    1	    0
#         1	      0	    1	    0	    1
#         1	      1	    0	    0	    1
#         1       1    	1	    1	    1

# Si on consid?re que S et la somme et C la retenue
# IN : A et B les nombre a additionner en format 1 bit plus une retenue venant d'un additionneur pr?c?dent Cin
# OUT : S la valeur de la somme sur 1 bit et C la retenue sur 1 bit
# EX : AdditionneurComplet(A=0,B=1,Cin=0) =>  c(1,0) avec un vecteur de format c(S,C)
# METHODE : On utilise deux demi additionneur en s?rie avec une porte OU pour calculer la retenue
# A et B sont branch?s sur le premier demi-Additionneur et donne S1 qui avec Cin sont branch?s sur le deuxi?me
# demi-additionneur. S et la sortie S2 du deuxi?me demi additionneur, et C et le r?sultat du porte OR
# entre les deux C1 et C2 des deux demi-additionneurs

# FONCTION ----------------------------------------------------------------
AdditionneurComplet <- function(NombreA, NombreB, Cin){
  result <- NULL
  if(!is.numeric(NombreA) | !is.numeric(NombreB) | !is.numeric(Cin)){
    result <- 'ONE PARAMETER IS NOT AN NUMERIC OR IS NA'
  } else {
    DemiAdditionneur(NombreA = NombreA, NombreB=NombreB) -> DemiAdd1
    DemiAdditionneur(NombreA = DemiAdd1[1], NombreB = Cin) -> DemiAdd2
    PorteLogiqueOR(Entree1=DemiAdd1[2],Entree2=DemiAdd2[2],Emetteur = 5) -> Cout
    result <- c(DemiAdd2[1],Retenue=Cout)
  }
  return(result)
}

# TESTS -------------------------------------------------------------------


# test la r?action attendue si la  fonction a un des param?tres d'entr?e qui n'est pas un numeric
AdditionneurComplet.test.Num <- function(){
  test_result = F
  if(identical(AdditionneurComplet(NombreA = as.character("Toto"),NombreB = 1,Cin = 1),'ONE PARAMETER IS NOT AN NUMERIC OR IS NA') &
     identical(AdditionneurComplet(NombreA = 1,NombreB = as.character("Toto"),Cin = 1),'ONE PARAMETER IS NOT AN NUMERIC OR IS NA') &
     identical(AdditionneurComplet(NombreA = 1,NombreB = 1,Cin = as.character("Toto")),'ONE PARAMETER IS NOT AN NUMERIC OR IS NA')){
    test_result = T
  }
  if(test_result){
    print("AdditionneurComplet.test.Num PASSED")
  }else {
    print("AdditionneurComplet.test.Num IS NOT PASSED")
  }
  return(test_result)
}

# test le comporement normal de la porte XOR
AdditionneurComplet.test.Value <- function(){
  test_result = F
  if(identical(AdditionneurComplet(NombreA =  0,NombreB = 0,Cin = 0),c(Somme=0,Retenue=0)) &
     identical(AdditionneurComplet(NombreA =  0,NombreB = 0,Cin = 1),c(Somme=5,Retenue=0)) &
     identical(AdditionneurComplet(NombreA =  0,NombreB = 1,Cin = 0),c(Somme=5,Retenue=0)) &
     identical(AdditionneurComplet(NombreA =  0,NombreB = 1,Cin = 1),c(Somme=0,Retenue=5)) &
     identical(AdditionneurComplet(NombreA =  1,NombreB = 0,Cin = 0),c(Somme=5,Retenue=0)) &
     identical(AdditionneurComplet(NombreA =  1,NombreB = 0,Cin = 1),c(Somme=0,Retenue=5)) &
     identical(AdditionneurComplet(NombreA =  1,NombreB = 1,Cin = 0),c(Somme=0,Retenue=5)) &
     identical(AdditionneurComplet(NombreA =  1,NombreB = 1,Cin = 1),c(Somme=5,Retenue=5))){
    test_result = T
  }
  if(test_result){
    print("AdditionneurComplet.test.Value PASSED")
  }else {
    print("AdditionneurComplet.test.Value IS NOT PASSED")
  }
  return(test_result)
}

#Enchaine tous les tests
AdditionneurComplet.test.AllTests <- function(){
  if(AdditionneurComplet.test.Num() &
     AdditionneurComplet.test.Value()){
    print(x = "ALL TESTS PASSED")
  } else print(x = "SOMETHING WENT WRONG !")
}

# Launch all tests
AdditionneurComplet.test.AllTests()
