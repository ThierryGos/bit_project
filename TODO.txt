20/11/2018  Thierry GOSTAN  Modifier l'additionneur N bits (du fichier BIT_10.R) pour qu'il soit cohérent avec une addition d'un chiffre négatif (ou fabriquer un soustracteur N bits). Dans tous les cas, prévoir l'addition d'un chiffre négatif, ce qui pour le moment n'est pas géré et renvoi un résultat non attendu.





DONE

22/11/2018 Thierry GOSTAN Modifier les scripts a partir du 07 pour tester le fonctionnement des portes dans le cas ou l'emetteur est sur un courant nul.

23/11/2018 Thierry GOSTAN Continuer de rajouter les commentaires roxygen2 pour que la documentation de tous les fichiers puisse être générée

23/11/2018 Thierry GOSTAN Modifier les tests des fonctions pour qu'ils utilisent la méthodologie et les outils du packages testthat
